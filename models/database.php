<?php
    $db_location = "localhost";
    $db_user = "root";
    $db_password = "";
    $db_name = "reimbursement";
    $mysqli = new mysqli($db_location,$db_user,$db_password,$db_name);

    // Check connection
    if ($mysqli -> connect_errno) {
        echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
        exit();
    }
?>