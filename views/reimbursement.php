<div class="card">
    <div class="card-body">
        <div class="card-title">
            <h5>Reimbursement</h5>
            <button type="button" class="btn btn-success button-link" content="reimbursement_form.php">Tambah
                Data</button>
        </div>
        <!-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> -->
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Keperluan</th>
                    <th scope="col">Tanggal</th>
                    <th scope="col">Total</th>
                    <th scope="col">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    if ($result){
                        $no = 0;
                        while($reimburse = mysqli_fetch_assoc($result)) {
                            $no++;
                ?>
                <tr>
                    <th scope="row"><?php echo $no;?></th>
                    <td><?php echo $reimburse['keperluan'];?></td>
                    <td><?php echo $reimburse['tanggal_keperluan'];?></td>
                    <td><?php echo $reimburse['sub_total'];?></td>
                    <td><?php echo $reimburse['status'];?></td>
                </tr>
                <?php
                        }
                    }
                    else{
                ?>
                <tr>
                    <td colspan="5" scope="row" class="text-center">Belum ada data tersedia</td>
                </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>

    </div>
</div>

<script>
$(document).ready(function () {

    $('.button-link').click(function () {
        var path = $(this).attr('content');
        var split = path.split('/');
        
        if (split.length > 1) {
            $.ajax({
                type: 'POST',
                data: {
                    path: split
                },
                url: '../handler/routes.php',
                dataType: 'html',
                success: function (response) {
                    $('#content').html(response);
                },
                error: function (xhr, textStatus, errorMessage) {
                    console.warn(textStatus);
                    console.warn(errorMessage);
                }
            })
        } else {
            $('#content').load(path);
        }
    });
})
</script>