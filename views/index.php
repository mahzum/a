<?php
  session_start();
  if (!$_SESSION) {
    header("location: login.php");
  }
?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet"
    href="<?php echo "http://".$_SERVER['SERVER_NAME'];?>/reimbursement/assets/css/bootstrap.min.css">

  <title>Reimbursement</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-primary bg-primary">
    <a class="navbar-brand text-white" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link text-white content-link" content="home.php" href="#">Home</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link text-white content-link" content="reimbursement/get" href="#">Reimbursement</a>
        </li>
      </ul>
      <div class="my-2 my-lg-0">
        <div class="btn-group dropleft">
          <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <?php echo $_SESSION['name']?>
          </button>
          <div class="dropdown-menu">
            <form method="post" action="../handler/login.php">
              <input type="hidden" name="action" value="logout">
              <button class="dropdown-item" type="submit">Logout</button>
            </form>
            <!-- <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Separated link</a> -->
          </div>
        </div>
      </div>
    </div>
  </nav>
  <div class="my-2 mx-2" id="content">
    
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <script src="<?php echo "http://".$_SERVER['SERVER_NAME'];?>/reimbursement/assets/js/bootstrap.min.js"></script>
  <script src="<?php echo "http://".$_SERVER['SERVER_NAME'];?>/reimbursement/assets/js/jquery-3.4.1.min.js"></script>
  <script>
    $(document).ready(function(){

      $('#content').load('home.php');
      $('.content-link').click(function(){
        var path = $(this).attr('content');
        var split = path.split('/');
        
        if(split.length > 1){
          $.ajax({
            type: 'POST',
            data: {
              path: split
            },
            url: '../handler/routes.php',
            dataType: 'html',
            success: function(response){
              $('#content').html(response);
            },
            error: function(xhr, textStatus, errorMessage){
              console.warn(textStatus);
              console.warn(errorMessage);
            }
          })
        }else{
          $('#content').load(path);
        }
      });
    })
  </script>
</body>
</html>
