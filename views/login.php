<!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->
<link rel="stylesheet" href="<?php echo "http://".$_SERVER['SERVER_NAME'];?>/reimbursement/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo "http://".$_SERVER['SERVER_NAME'];?>/reimbursement/assets/css/custom_login.css">

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo "http://".$_SERVER['SERVER_NAME'];?>/reimbursement/assets/js/bootstrap.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <img src="<?php echo "http://".$_SERVER['SERVER_NAME'];?>/reimbursement/assets/images/thin.svg" class="my-3" width="50" height="50" id="icon" alt="User Icon" />
    </div>

    <!-- Login Form -->
    <form method="post" action="../handler/login.php">
      <input type="hidden" name="action" value="login">
      <input type="text" id="login" class="fadeIn second" name="login" placeholder="login">
      <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>

    <!-- Remind Passowrd -->
    <!-- <div id="formFooter">
      <a class="underlineHover" href="#">Forgot Password?</a>
    </div> -->

  </div>
</div>