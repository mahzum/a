<div class="card">
    <div class="card-body">
        <form action="javascript:void(0)" id="header">
            <div class="card-title">
                <h5>Reimbursement</h5>
                <button type="submit" class="btn btn-primary">Simpan Data</button>
                <button type="button" class="btn btn-cancel button-link">Batal</button>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Keperluan</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control-plaintext" name="keperluan" placeholder="Nama Keperluan" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Tanggal Keperluan</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="tanggal_keperluan" required>
                </div>
            </div>
        </form>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Foto</th>
                    <th scope="col">Total</th>
                    <th scope="col">#</th>
                </tr>
            </thead>
            <tbody id="line-content">
            </tbody>
            <tbody>
                <tr>
                    <td colspan="3">
                        <button class="btn btn-link" data-toggle="modal" data-target="#modalLine">Tambah Data</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalLine" tabindex="-1" role="dialog" aria-labelledby="modalLineLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="javascript:void(0)" id="line">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Rincian Reimbursement</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Foto Nota</label>
                        <div class="col-sm-10">
                            <input type="file" accept="image/*" name="file" id="line_file" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" required>Total</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" step="0.01" id="line_total" name="total" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<?php echo "http://".$_SERVER['SERVER_NAME'];?>/reimbursement/assets/js/jquery-3.4.1.min.js"></script>
<script src="<?php echo "http://".$_SERVER['SERVER_NAME'];?>/reimbursement/assets/js/bootstrap.min.js"></script>
<script>
    var formdata = new FormData();
    var lines = [];
    $('form#line').on('submit', function(event){
        event.preventDefault();
        var idx = lines.length;
        var formdata = new FormData(this);
        formdata.append('path', "reimbursement_line/create");
        formdata.append('file', $("#line_file").prop('files')[0]);

        var line_value = {
            'file' : $("#line_file").prop('files')[0],
            'total' : $("#line_total").val(),
        };
        $.ajax({
            enctype: 'multipart/form-data',
            type: 'POST',
            data: formdata,
            url: '../handler/routes.php',
            // dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            success : function(response){
                $('#line-content').append(response);
                lines.push(response['id']);
                $('form#line').trigger('reset');
            },
            error: function(xhr, status, message){
                console.warn(status);
                console.warn(message);
            }
        })
        
        // console.log(lines);
        
        // $('#modalLine').modal('hide');
    });
    function delete_line(line_id){
        delete lines[line_id];
        $('#'+line_id).remove();
    }
    $('form#header').on('submit', function(event){
        event.preventDefault();
        
        formdata.append('path', "reimbursement/create");
        formdata.append('lines', "<?php echo json_encode("+lines+")?>");
        
        $.ajax({
            enctype: 'multipart/form-data',
            type: 'POST',
            data: formdata,
            url: '../handler/routes.php',
            // dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            success : function(response){
                console.log(response['id']);
            },
            error: function(xhr, status, message){
                console.warn(status);
                console.warn(message);
            }
        })
            // data: {
            //     data : formdata,
            //     lines: lines,
            //     path : 'reimbursement/create'
            // },
    });
</script>