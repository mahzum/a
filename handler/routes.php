<?php
    include "../models/_include.php";
    
    $path = $_POST['path'];
    if (!is_array($path)){
        $path = explode('/', $_POST['path']);        
    }
    $class = $path[0];
    $action = $path[1];
    $object = new $class();
    $object->$action();
?>