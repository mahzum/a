<?php
    require "../models/login.php";
    $session = new UserSession();
    if ($_POST || $_GET) {
        $action = ($_POST['action'] != null) ? $_POST['action'] : $_GET['action'];
        switch ($action) {
            case 'login':
                $result = $session->login($_POST['login'], $_POST['password']);
                echo $action;
    
                if ($result) {
                    header("location: ../views/index.php");
                }
                break;
            
            case 'logout':
                echo $action;
                $session->logout();
            break;
            
            default:
                # code...
                break;
        }
    }
    else{
        header("location:javascript://history.go(-1)");
    }
?>